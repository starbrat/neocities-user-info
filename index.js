var express = require('express');
var app = express();
const fetch = require('node-fetch');

app.get('/', function (req, res) {
	var data = {
		error : "No id specified"
	};
	if (req.query.id) {
		fetch('https://neocities.org/api/info?sitename='+req.query.id)
	    .then(res => res.json())
	    .then(body => {
	    	console.log(body);
	    	if (body.result=="success") {
	    		data = {
	    			sitename : body.info.sitename,
	    			views : body.info.views,
	    			hits : body.info.hits,
	    			created_at : body.info.created_at,
	    			last_updated : body.info.last_updated,
	    			domain : body.info.domain,
	    			tags : body.info.tags,
	    			latest_ipfs_hash : body.info.latest_ipfs_hash
	    		};
	    	}
			else{
				data = {
					error : "Request failed"
				};
			}
			res.send(data);
	    });
	}
});
app.get('/jsonp/', function (req, res) {
	var data = {
		error : "No id specified"
	};
	if (req.query.id) {
		fetch('https://neocities.org/api/info?sitename='+req.query.id)
	    .then(res => res.json())
	    .then(body => {
	    	console.log(body);
	    	if (body.result=="success") {
	    		data = {
	    			sitename : body.info.sitename,
	    			views : body.info.views,
	    			hits : body.info.hits,
	    			created_at : body.info.created_at,
	    			last_updated : body.info.last_updated,
	    			domain : body.info.domain,
	    			tags : body.info.tags,
	    			latest_ipfs_hash : body.info.latest_ipfs_hash
	    		};
	    	}
			else{
				data = {
					error : "Request failed"
				};
			}
			res.jsonp(data);
	    });
	}
});

var port = process.env.PORT || 3001;
var host = '0.0.0.0';
var server = app.listen(port, host, function(){
	console.log("App started...");
});
