# README #

This is an adaptation of the discord-user-info repo to draw info from Neocities instead of Discord.
Start the server and navigate to localhost:3001/?id=user_id to get user data.
Note: When hosting on Heroku, make sure to specify host as '0.0.0.0', otherwise your bot may crash.